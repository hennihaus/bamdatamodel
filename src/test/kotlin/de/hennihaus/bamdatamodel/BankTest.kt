package de.hennihaus.bamdatamodel

import com.fasterxml.jackson.module.kotlin.readValue
import de.hennihaus.bamdatamodel.objectmothers.BankObjectMother.getSyncBank
import de.hennihaus.bamdatamodel.testutils.bamObjectMapper
import de.hennihaus.bamdatamodel.testutils.getResourceAsText
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class BankTest {
    @Test
    fun `should deserialize correctly json into bank`() {
        val json = getResourceAsText(path = "/bank.json")

        val result: Bank = bamObjectMapper().readValue(content = json)

        assertEquals(expected = getSyncBank(), actual = result)
    }

    @Test
    fun `should serialize correctly a bank into json`() {
        val bank = getSyncBank()

        val result: String = bamObjectMapper().writeValueAsString(bank)

        assertEquals(expected = getResourceAsText(path = "/bank.json"), actual = result)
    }
}
