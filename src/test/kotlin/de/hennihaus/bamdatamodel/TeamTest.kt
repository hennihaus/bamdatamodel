package de.hennihaus.bamdatamodel

import com.fasterxml.jackson.module.kotlin.readValue
import de.hennihaus.bamdatamodel.objectmothers.TeamObjectMother.getFirstTeam
import de.hennihaus.bamdatamodel.testutils.bamObjectMapper
import de.hennihaus.bamdatamodel.testutils.getResourceAsText
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class TeamTest {
    @Test
    fun `should deserialize correctly json into team`() {
        val json = getResourceAsText(path = "/team.json")

        val result: Team = bamObjectMapper().readValue(content = json)

        assertEquals(expected = getFirstTeam(), actual = result)
    }

    @Test
    fun `should serialize correctly a team into json`() {
        val team = getFirstTeam()

        val result: String = bamObjectMapper().writeValueAsString(team)

        assertEquals(expected = getResourceAsText(path = "/team.json"), actual = result)
    }
}
