package de.hennihaus.bamdatamodel

import com.fasterxml.jackson.module.kotlin.readValue
import de.hennihaus.bamdatamodel.objectmothers.StatisticObjectMother.getFirstTeamAsyncBankStatistic
import de.hennihaus.bamdatamodel.testutils.bamObjectMapper
import de.hennihaus.bamdatamodel.testutils.getResourceAsText
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class StatisticTest {
    @Test
    fun `should deserialize correctly json into statistic`() {
        val json = getResourceAsText(path = "/statistic.json")

        val result: Statistic = bamObjectMapper().readValue(content = json)

        assertEquals(expected = getFirstTeamAsyncBankStatistic(), actual = result)
    }

    @Test
    fun `should serialize correctly a statistic into json`() {
        val statistic = getFirstTeamAsyncBankStatistic()

        val result: String = bamObjectMapper().writeValueAsString(statistic)

        assertEquals(expected = getResourceAsText(path = "/statistic.json"), actual = result)
    }
}
